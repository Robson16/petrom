export default class Navbar {
	constructor() {
		this.wpadminbar = document.querySelector("#wpadminbar");
		this.header = document.querySelector("#header");
		this.navbar = document.querySelector("#navbar");
		this.main = document.querySelector("main");

		this.viwewportX = null;
		this.offset = null;

		this.events();
	}

	// Events
	events() {
		["scroll"].forEach((event) => {
			window.addEventListener(event, () => {
				this.handleStickEffect();
			});
		});

		["load", "resize"].forEach((event) => {
			window.addEventListener(event, () => {
				this.setOffset();
				this.setViwewportSize();
				this.mainContentSpaceTop();
			});
		});
	}

	// Methods
	handleStickEffect() {
		if (window.scrollY > this.offset) {
			this.header.classList.add("sticky");

			// Applies to landscape phones, 576px and up
			if (this.wpadminbar && this.viwewportX > 576) {
				this.navbar.style.top = this.wpadminbar.offsetHeight + "px";
			}
		} else {
			this.header.classList.remove("sticky");

			// Applies to landscape phones, 576px and up
			if (this.wpadminbar && this.viwewportX > 576) {
				this.navbar.style.top = "initial";
			}
		}
	}

	setOffset() {
		this.offset = this.wpadminbar ? this.wpadminbar.offsetHeight : 0;
	}

	setViwewportSize() {
		this.viwewportX = document.documentElement.clientWidth;
	}

	mainContentSpaceTop() {
		this.main.style.paddingTop = this.header.offsetHeight + "px";
	}
}
