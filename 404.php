<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<div class="error-code" style="background-image: url('<?php echo get_template_directory_uri() . '/assets/images/bg-404.jpg'; ?>">
		<div class="container">
			<span>404</span>
		</div>
	</div>
	<div class=" error-message">
		<div class="container">
			<p><?php _e('We could not find the page you are looking for.', 'petrom'); ?></p>
			<a href="<?php echo get_home_url(); ?>"><?php _e('Home page', 'petrom'); ?></a>
		</div>

	</div>
</main>

<?php
get_footer();
