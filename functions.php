<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

//
function petrom_scripts()
{
	// CSS
	wp_enqueue_style('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', null, '5.1.3', 'all');
	wp_enqueue_style('petrom-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('petrom-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('petrom-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('popper', '//cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js', array('jquery'), '2.10.2', true);
	wp_enqueue_script('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js', array('jquery', 'popper'), '5.1.3', true);
	wp_enqueue_script('petrom-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'petrom_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function petrom_gutenberg_scripts()
{
	wp_enqueue_style('petrom-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'petrom_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function petrom_setup()
{
	// Enabling translation support
	$textdomain = 'petrom';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 52,
		'width'       => 161,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'petrom'),
		'footer_menu' => __('Footer Menu', 'petrom'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'petrom'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Platinum', 'petrom'),
			'slug'  => 'platinum',
			'color' => '#e4e2e2',
		),
		array(
			'name'  => __('Royal Blue Dark', 'petrom'),
			'slug'  => 'royal-blue-dark',
			'color' => '#02216a',
		),
		array(
			'name'  => __('Cyber Yellow', 'petrom'),
			'slug'  => 'cyber-yellow',
			'color' => '#ffce00',
		),
		array(
			'name'  => __('Black Chocolate', 'petrom'),
			'slug'  => 'black-chocolate',
			'color' => '#26211d',
		),
		array(
			'name'  => __('Black', 'petrom'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'petrom'),
			'size' => 12,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'petrom'),
			'size' => 16,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'petrom'),
			'size' => 20,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'petrom'),
			'size' => 32,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'petrom'),
			'size' => 48,
			'slug' => 'huge',
		),
	));

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/media-text', array(
		'name' => 'icon-text',
		'label' => __('Icon and Text', 'petrom'),
	));
	register_block_style('core/list', array(
		'name' => 'arrow',
		'label' => __('Arrow', 'petrom'),
	));
	register_block_style('core/group', array(
		'name' => 'yellow-border',
		'label' => __('Yellow Border', 'petrom'),
	));
}
add_action('after_setup_theme', 'petrom_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function petrom_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'petrom'),
		'id' => 'petrom-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'petrom'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'petrom'),
		'id' => 'petrom-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'petrom'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'petrom'),
		'id' => 'petrom-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'petrom'),
	)));


	// Barra Lateral Blog
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'petrom'),
		'id' => 'petrom-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'petrom'),
	)));
}
add_action('widgets_init', 'petrom_sidebars');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 * Custom Post Type
 */
require_once get_template_directory() . '/includes/post-types/team-posttype.php';

/**
 * Custom Shortcode
 */
require_once get_template_directory() . '/includes/shortcode/team-shortcode.php';

/**
 * Custom Meta Boxes
 */
require_once get_template_directory() . '/includes/meta-boxes/team-metabox.php';
