<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('petrom-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('petrom-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('petrom-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('petrom-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('petrom-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('petrom-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>

		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="container">
		<?php
		wp_nav_menu(array(
			'theme_location' => 'footer_menu',
			'depth' => 1,
			'container_class' => 'footer-menu-wrap',
			'menu_class' => 'footer-menu',
		));
		?>
		<!-- /.footer-menu -->
	</div>

	<div class="footer-copyright">
		<div class="container ">
			<span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;&ndash;&nbsp;<?php _e('All rights reserved.', 'petrom') ?></span>
			<span>
				<?php _e('Developed by:', 'petrom') ?>
				&nbsp;
				<a href="https://vollup.com/" target="_blank">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/vollup-brand-white.png' ?>" alt="Vollup">
				</a>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
