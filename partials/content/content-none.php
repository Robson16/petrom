<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div class="col">
	<h3><?php _e('No content to display', 'petrom'); ?></h3>
</div>
