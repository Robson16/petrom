<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
	<?php
	while (have_posts()) {
		the_post();

		get_template_part('partials/content/content', 'single');

		the_post_navigation(
			array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next Post', 'petrom') . '</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous Post', 'petrom') . '</span>',
			)
		);
	}
	?>
</main>

<div class="container">
	<?php if (comments_open() || get_comments_number()) comments_template(); ?>
</div>
<!--/.container-->

<?php
get_footer();
