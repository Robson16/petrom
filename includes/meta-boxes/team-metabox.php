<?php

function your_prefix_register_meta_boxes($meta_boxes)
{
	$prefix = 'petrom_team_';

	$meta_boxes[] = [
		'title'    => esc_html__('Team extra fields', 'petrom'),
		'id'       => 'petrom_team_fields',
		'post_types' => ['team'],
		'context'  => 'after_title',
		'autosave' => true,
		'fields'   => [
			[
				'type'        => 'text',
				'name'        => esc_html__('Sub Title', 'petrom'),
				'id'          => $prefix . 'sub_title',
				'placeholder' => esc_html__('Sub Title', 'petrom'),
			],
			[
				'type'        => 'text',
				'name'        => esc_html__('Role', 'petrom'),
				'id'          => $prefix . 'role',
				'placeholder' => esc_html__('Role', 'petrom'),
			],
		],
	];

	return $meta_boxes;
}
add_filter('rwmb_meta_boxes', 'your_prefix_register_meta_boxes');
