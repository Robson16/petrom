<?php

/**
 * Register Team Custom Post
 */

function petrom_team_post_type()
{
	$labels = array(
		'name'                  => _x('Team', 'Post Type General Name', 'petrom'),
		'singular_name'         => _x('Team', 'Post Type Singular Name', 'petrom'),
		'menu_name'             => __('Team', 'petrom'),
		'name_admin_bar'        => __('Team', 'petrom'),
		'archives'              => __('Team Archives', 'petrom'),
		'attributes'            => __('Item Attributes', 'petrom'),
		'parent_item_colon'     => __('Parent Item:', 'petrom'),
		'all_items'             => __('All Items', 'petrom'),
		'add_new_item'          => __('Add New Item', 'petrom'),
		'add_new'               => __('Add New', 'petrom'),
		'new_item'              => __('New Item', 'petrom'),
		'edit_item'             => __('Edit Item', 'petrom'),
		'update_item'           => __('Update Item', 'petrom'),
		'view_item'             => __('View Item', 'petrom'),
		'view_items'            => __('View Items', 'petrom'),
		'search_items'          => __('Search Item', 'petrom'),
		'not_found'             => __('Not found', 'petrom'),
		'not_found_in_trash'    => __('Not found in Trash', 'petrom'),
		'featured_image'        => __('Featured Image', 'petrom'),
		'set_featured_image'    => __('Set featured image', 'petrom'),
		'remove_featured_image' => __('Remove featured image', 'petrom'),
		'use_featured_image'    => __('Use as featured image', 'petrom'),
		'insert_into_item'      => __('Insert into item', 'petrom'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'petrom'),
		'items_list'            => __('Items list', 'petrom'),
		'items_list_navigation' => __('Items list navigation', 'petrom'),
		'filter_items_list'     => __('Filter items list', 'petrom'),
	);
	$args = array(
		'label'                 => __('Team', 'petrom'),
		'description'           => __('Team members', 'petrom'),
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('team', $args);
}
add_action('init', 'petrom_team_post_type', 0);
