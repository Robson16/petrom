<?php // Sidebar Blog
?>

<aside class="blog-sidebar">
    <?php if (is_active_sidebar('petrom-sidebar-blog')) dynamic_sidebar('petrom-sidebar-blog'); ?>
</aside>
